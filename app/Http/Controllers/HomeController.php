<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use App\User;
use Illuminate\Support\Facades\Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
        
        $this->middleware('auth',['except' => ['login','demo']]);

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */


    public function logout(){
        User::where('activo',1)
           ->where('id',Auth::user()->id)
           ->update([
               'is_logged' =>0
           ]);
        Auth::logout();
        return view('welcome');
    }

    public function login(Request $request)
    {
      
        $email = $request->input('email');
        $password = $request->input('password');

        if (Auth::attempt(['email' => $email, 'password' => $password, 'activo' => 1, 'is_logged' => 0])) {
            // Authentication passed...
           //hacemos el cambio de status 
           

           User::where('activo',1)
           ->where('id',Auth::user()->id)
           ->update([
               'is_logged' =>1
           ]);

            return redirect()->intended('home');
        }
        else{
            $mensaje = 'credenciales erroneas o session ya iniciada.';
            return redirect()->back()->with('mensaje', $mensaje);
            //return redirect('/home')->with('mensaje',$mensaje);
        }
    }

    public function demo()
    {
        echo ' si llego';
    }

}
