<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Storage;

class respaldoController extends Controller
{
    public function vistaResplados()
    {
        $lista_archivos = $this->getAllFiles();
        return view('respaldos', compact('lista_archivos'));
    }

    public function subirArchivo(Request $request)
    {
        try {

            if ($request->hasFile('archivo')) {

                $files = $request->file('archivo');

                foreach ($files as $file) {

                    $nombre = $file->getClientOriginalName();

                    $nombre = pathinfo($nombre, PATHINFO_FILENAME);
                    $extension = $file->getClientOriginalExtension();
                    $nombre = $nombre . "_" . date("Y-m-d H-i-s") . "." . $extension;
                    Storage::disk('local')->putFileAs('', $file, $nombre, 'public');
                }
            } else {
                //poner que regrese mensaje de error
                $mensaje = 'archivo no seleccionado o invalido.';
                //return redirect()->back()->with('mensaje', $mensaje);
            }
        } catch (\RunTimeException $e) {
            return $e;
            $mensaje = 'Error cargando archivo';
            // return redirect()->back()->with('mensaje', $mensaje);
        }

    }

    public function getAllFiles()
    {
        $allFiles = Storage::disk('local')->allFiles();
        $files = [];
        foreach ($allFiles as $file) {
            $files[] = $this->fileInfo(pathinfo(env('RESPALDOS_PATH') . $file));
        }
        return json_encode(['data' => $files]);
        //return $files;
    }

    public function fileInfo($filePath)
    {
        $file = array();
        $file['name'] = $filePath['filename'];
        $file['extension'] = $filePath['extension'];
        $file['size'] = filesize($filePath['dirname'] . '/' . $filePath['basename']);
        $file['size'] = $this->FileSizeConvert($file['size']);
        $file['path_descarga'] = '/downloadFile/' . $file['name'] . '.' . $file['extension'];

        return $file;
    }

    public function downloadFile($file_name)
    {
        $filePath = env('RESPALDOS_PATH') . $file_name;
        if (file_exists($filePath)) {
            return Response::download($filePath);
        } else {
            // Error
            exit('Requested file does not exist on our server!');
        }
    }

    public function FileSizeConvert($bytes)
    {
        $bytes = floatval($bytes);

        $arBytes = array(
            0 => array(
                "UNIT" => "TB",
                "VALUE" => pow(1024, 4),
            ),
            1 => array(
                "UNIT" => "GB",
                "VALUE" => pow(1024, 3),
            ),
            2 => array(
                "UNIT" => "MB",
                "VALUE" => pow(1024, 2),
            ),
            3 => array(
                "UNIT" => "KB",
                "VALUE" => 1024,
            ),
            4 => array(
                "UNIT" => "B",
                "VALUE" => 1,
            ),
        );

        foreach ($arBytes as $arItem) {
            if ($bytes >= $arItem["VALUE"]) {
                $result = $bytes / $arItem["VALUE"];
                $result = str_replace(".", ",", strval(round($result, 2))) . " " . $arItem["UNIT"];
                break;
            } else {
                $result = '0 B';
            }
        }
        return $result;
    }

}
