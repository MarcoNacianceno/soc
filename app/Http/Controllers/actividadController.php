<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\logActividades;
use DB;

class actividadController extends Controller
{
    
    public function vistaActividades(){
        try{        
            // $actividad = logActividades::orderBy('idActividad','desc')
            // ->take(10)
            // ->get();
        return view('actividades'/*,compact('actividad')*/);
        }
        catch(Exception $e){
            return $e;
        }

    }

    public function consultasXfechas(Request $req){
        try{
            $startDate = $req->input('startDate');
            $endDate = $req->input('endDate');

            $startDate .= " 00:00:00";
            $endDate .= " 23:59:59";

            $respuesta = array();
            //logActividades::where('created_at','>=',$startDate)->where('created_at','<=',$endDate)->select('*')->get();
            //$respuesta[0]  = logActividades::where('created_at','>=',$startDate)->where('created_at','<=',$endDate)->get();

            $respuesta[0]  = DB::table('log_actividades')
            ->join('users', 'log_actividades.idUser', '=', 'users.id')
            ->select('users.name','users.id', 'log_actividades.*')
            ->where('log_actividades.created_at','>=',$startDate)
            ->where('log_actividades.created_at','<=',$endDate)->get();

            return $respuesta[0];

            
        }
            catch(Exception $e){

            }        
    }
}
