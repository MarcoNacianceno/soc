<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\logActividades;
use Auth;


class usuarioController extends Controller
{
    //controlador para usuario

    public function vista(){
        return view('/registroUsuario');
    }

    public function allUsuarios(){
        $usuarios = User::where('activo',1)->get();
        return view('usuarios',compact('usuarios'));
    }

    public function miperfil(){
        try{
            return view('miperfil');
        }
        catch(Exception $e){
            return $e;

        }
    }

    public function obtenerUsuarioAdmin($id){
        try{
             $usuario = User::where('activo',1)->where('id',$id)->first();
             return view('editarUsuario',compact('usuario'));
        }
        catch(Exception $e){
            return $e;
        }
    }

    public function obtenerUsuario(){
        try{
             $usuario = Auth::user();
             return view('editarUsuario',compact('usuario'));
        }
        catch(Exception $e){
            return $e;
        }
    }

    public function agregarUsuario(Request $req){
        $name = $req-> input ('name');
        $email = $req->input('email');
        $password = Hash::make($req->input('password'));
        try{
            $user = new User;
            $user ->name = $name; 
            $user ->email = $email;
            $user ->password = $password;
            $user ->activo = true;
            $user ->id_rol = 2;
            $user -> save();

            $actividad = new logActividades;
            $actividad ->actividad = 'Creacion de Usuario ';
            $actividad ->descripcion = 'Se creo el usuario: ' . $name . ' con el correo: ' . $email;
            $actividad ->idUser = Auth::user()->id;

            $actividad ->save();
    
            return redirect('usuarios');
            
        }
        catch(Exception $e){
            return $e;
        }
    }
    
    public function actualizarUsuario(Request $req){
        try{
            User::where('activo',1)
            ->where('id',$req->id)
            ->update([
                'name' =>$req->name,
                'email'=>$req->email,
                'password'=>Hash::make($req->password)
            ]);

            $actividad = new logActividades;
            $actividad ->actividad = 'Actualizacion de Usuario';
            $actividad ->descripcion = 'Se actualizo el usuario : ' .  $req -> input('name');
            $actividad ->idUser = Auth::user()->id;

            $actividad ->save();


            return redirect('home');

        }
        catch(Exception $e){
            return $e;
        }
    }


    public function borrarUsuario(Request $req){
        try{
            $id = $req->input('id');

            $usuario_temp = User::where('activo',1)
            ->where('id',$id)->first();
                       
            User::where('activo',1)
            ->where('id',$id)
            ->update([
                'activo'=>false
            ]);

            $actividad = new logActividades;
            $actividad ->actividad = 'Eliminacion de cliente';
            $actividad ->descripcion = 'Se elimino el usuario: ' . $usuario_temp->name ;
            $actividad ->idUser = Auth::user()->id;

            $actividad ->save();

            return 1;
            
        }
        catch(Exception $e){
            return $e;
        }
    }
}
