<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use App\logActividades;
use App\Contacto;
use Auth;
use DB;
use Storage;

class clientesController extends Controller
{

   //funcion para ir al registro

    public function vista(){
        return view('registroCliente');
    }

    public function index()
    {   


        $clientes = DB::table('clientes')
        ->join('contacto','contacto.idCliente','=','clientes.idCliente')
        ->select('clientes.*','contacto.*')
        ->where('activo',1)
        ->get();

        // $clientes = Cliente::where('activo',1)->get();
        return view('home',compact('clientes'));



    }

  


    public function obtenerCliente($idCliente){
        try{
                        
            // $cliente = Cliente::where('activo',1)->where('idCliente',$idCliente)->first();
            //  return view('editarCliente',compact('cliente'));

             $cliente = DB::table('clientes')
             ->join('contacto','contacto.idCliente','=','clientes.idCliente')
             ->select('clientes.*','contacto.*')
             ->where('clientes.idCliente' , $idCliente)
             ->first();
             return view('editarCliente',compact('cliente'));
        }
        catch(Exception $e){
            return $e;
        }
    }



    //funcion para agregar clientes
    public function agregarCliente(Request $req){
        $nombre = strtoupper($req -> input('nomCliente'));
        $sucursal = strtoupper($req -> input ('sucursal'));
        $dir = strtoupper($req -> input('ip_url'));
        $fabricante = strtoupper($req -> input ('fabricante'));
        $serial = strtoupper($req -> input ('serial'));
        $notas = ucfirst($req -> input ('notas'));

        $nombreContacto = strtoupper($req -> input ('nomContactoCliente'));
        $telContacto = strtoupper($req -> input ('telContactoCliente'));
        $emailContacto = $req -> input ('emailContactoCliente');
        
        


        try{
            $cliente = new Cliente;
            $cliente ->nomCliente = $nombre;
            $cliente ->sucursal = $sucursal;
            $cliente ->dirIP = $dir;
            $cliente ->fabricante = $fabricante;
            $cliente ->serial = $serial;
            $cliente ->notas = $notas;
            $cliente->activo = true;
    
            $cliente ->save();

            $idCliente = $cliente->id;

            $contacto = new Contacto;
            $contacto ->nomContacto = $nombreContacto;
            $contacto ->telContacto = $telContacto;
            $contacto ->emailContacto = $emailContacto;
            $contacto ->idCliente = $idCliente;

            $contacto -> save();

            
            $actividad = new logActividades;
            $actividad ->actividad = 'Creacion de Cliente';
            $actividad ->descripcion = 'Se Creo el cliente: ' . $nombre . ' con la sucursal: ' . $sucursal;
            $actividad ->idUser = Auth::user()->id;
            $actividad ->save();
    
            return redirect('home');
        }
        
        catch(Exception $e){
            return $e;
        }
    }

    //Funcion para actualizar el cliente

    public function actualizarCliente(Request $req){
        try{
            Cliente::where('activo',1)
            ->where('idCliente', $req->idCliente)
            ->update([
                'nomCliente' => strtoupper($req ->nomCliente),
                'sucursal' => strtoupper($req ->sucursal),
                'dirIP' => strtoupper($req ->ip_url),
                'fabricante' => strtoupper($req ->fabricante),
                'serial' => strtoupper($req ->serial),
                'notas' =>ucfirst($req ->notas)
            ]);

            Contacto::where('contactoActivo',1)
            ->where('idCliente', $req->idCliente)
            ->update([
                'nomContacto' => strtoupper($req ->nomContactoCliente),
                'telContacto' => strtoupper($req ->telContactoCliente),
                'emailContacto' => $req ->emailContactoCliente,
            ]);                   

            $actividad = new logActividades;
            $actividad ->actividad = 'Actualizacion de Cliente';
            $actividad ->descripcion = 'Se actualizo el cliente : ' .  $req -> input('nomCliente');
            $actividad ->idUser = Auth::user()->id;

            $actividad ->save();


            return redirect('home');
        }
            catch(Exception $e){
                return $e;
            }
    }

    //funcion para borrar el cliente
    public function borrarCliente(Request $req){
        try{
            $id_cliente = $req->input('idCliente');

            $cliente_temp = Cliente::where('activo',1)
            ->where('idCliente',$id_cliente)->first();
            
            Cliente::where('activo',1)
            ->where('idCliente',$id_cliente)
            ->update([
                'activo'=>false
            ]);

            $actividad = new logActividades;
            $actividad ->actividad = 'Eliminacion de Cliente';
            $actividad ->descripcion = 'Se elimino el cliente: ' . $cliente_temp->nomCliente ;
            $actividad ->idUser = Auth::user()->id;

            $actividad ->save();

            return 1;
        }
        catch(Exception $e){
            return $e;
        }
    }



}
