<?php

namespace App\Http\Middleware;

use \App\User;
use Closure;
use Auth;

class RefreshLastActivityTime
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->is_logged == 0)
        {
            return redirect('logout')->with('mensaje','Sesion terminada por inactividad.');
        }
        else
        {
            User::where('activo', 1)
            ->where('id', Auth::user()->id)
            ->update([
                'lastActivityTime' => time(),
            ]);
        \Log::info('lastActivityTime: ' . time());
        }
       
        return $next($request);
    }
}
