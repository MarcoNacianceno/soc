<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->id_rol != 1)
        {
            $mensaje = 'Privilegios insuficientes.';
            return redirect('/home')->with('mensaje',$mensaje);
        }
        return $next($request);
    }
}
