<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Session;
use \App\User;

class CheckSessionTimeLife
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    protected $session;
    //protected $timeout = 1200;
    protected $timeout = 10;

    public function __construct(\Illuminate\Session\Store $session)
    {
        $this->session = $session;
    }
  
    public function handle($request, Closure $next)
    {

        \Log::info('CheckSessionTimeLife......');
        $timeout = config('session.lifetime', 5);
        // $minutes = 1;
        // $timeout = config('session.lifetime', $minutes) * 1;

        if (Session::has('lastActivityTime') && (time() - Session::get('lastActivityTime')) > $timeout) {
            if (Auth::check()) {
                User::where('activo', 1)
                    ->where('id', Auth::user()->id)
                    ->update([
                        'is_logged' => 0,
                    ]);
                Session::flush();
                Auth::logout();
                \Log::info('Session cerrada......');
                return redirect('login');
            }
        }
        Session::put('lastActivityTime', time());
        \Log::info('OK......');
        return $next($request);

    }
}
