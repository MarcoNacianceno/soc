<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class logActividades extends Model
{
    //
    protected $table = 'log_actividades';

    protected $fillable = [
        'actividad',
        'descripcion',
        'idUser'
    ];

    //este modelo se utiliza en el clientesController
}
