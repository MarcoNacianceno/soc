<?php

namespace App\Console\Commands;
use DB;
use Illuminate\Console\Command;

class VerifyInactivity extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:verifyinactivity';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Kill session of users with inactivity';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->info('Cron VerifyActivity');
        $users = DB::table('users')
        ->where('is_logged',1)
        ->where('activo',1)
        ->whereNotNull('lastActivityTime')
        ->get();

        //ahora que tenemos todos los usuarios activos verificamos su ultimo movimiento
        $segundos = config('session.lifetime') * 60; // tiempo establecido en archivo .env 

        foreach ($users as $key => $user) {

            if(time() - $user->lastActivityTime > $segundos)
            {
            
                DB::table('users')->where('id',$user->id)
                ->update([
                    'is_logged'=>0,
                ]);

                $this->info('Usuario ' .$user->id . ' eliminado');
            }
        }
        
    }
}
