<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/logout','HomeController@logout')->name('logout');
Route::post('/login_1','HomeController@login')->name('login_1');


Auth::routes();


Route::group(['middleware'=>['auth','isAdmin','refreshlastactivity']],function()
{
    Route::get('/actividades','actividadController@vistaActividades');
    Route::post('/fechasActividad','actividadController@consultasXfechas');
    Route::get('/registroCliente','clientesController@vista');
    Route::get('/registroUsuario','usuarioController@vista');
    Route::get('/usuarios','usuarioController@allUsuarios');
    Route::get('/editarCliente/{idCliente}','clientesController@obtenerCliente');
    Route::post('/clientEditado','clientesController@actualizarCliente');
    Route::post('/borrarCliente','clientesController@borrarCliente');
    Route::post('/regCliente','clientesController@agregarCliente');
    Route::post('/agregarUsuario','usuarioController@agregarUsuario');
    Route::get('/editarUsuario/{id}','usuarioController@obtenerUsuarioAdmin');
  
    Route::post('/borrarUsuario','usuarioController@borrarUsuario');

    Route::get('/respaldos','respaldoController@vistaResplados');
    Route::get('/getAllFiles','respaldoController@getAllFiles');
    Route::get('/downloadFile/{pathFile}','respaldoController@downloadFile');

    Route::post('/subirArchivo','respaldoController@subirArchivo');
    

});


Route::group(['middleware'=>['auth','refreshlastactivity']  ],function(){
    Route::get('/home','clientesController@index')->name('home');
    Route::get('/editarUsuario','usuarioController@obtenerUsuario');
    Route::post('/usuarioEditado','usuarioController@actualizarUsuario');
    Route::get('/micuenta','usuarioController@miPerfil');

//Rutas de Clientes


    Route::get('/clientes','clientesController@allCliente');
   
    //Route::get('/registroCliente','clientesController@vista');
    


    //Rutas para usuarios
    //Route::get('/registroUsuario','usuarioController@vista');
    
   

    //Rutas para actvidades
    //Route::get('/actividades','actividadController@vistaActividades');
    //Route::post('/fechasActividad','actividadController@consultasXfechas');
});

