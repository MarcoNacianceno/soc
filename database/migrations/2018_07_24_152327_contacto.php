<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Contacto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacto', function (Blueprint $table) {
            $table->increments('idContacto');
            $table->string('nomContacto');
            $table->string('telContacto');
            $table->string('emailContacto');
            $table->unsignedInteger('idCliente');
            $table->timestamps();

            $table->foreign('idCliente')
            ->references('idCliente')
            ->on('clientes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacto');
    }
}
