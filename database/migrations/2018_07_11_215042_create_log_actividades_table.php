<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogActividadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_actividades', function (Blueprint $table) {
            $table->increments('idActividad');
            $table->string('actividad');
            $table->string('descripcion');
            $table->unsignedInteger('idUser');
            $table->timestamps();

            $table->foreign('idUser')
                    ->references('id')
                    ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_actividades');
    }
}
