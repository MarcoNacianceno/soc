<?php

use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
    }
}

class UsersTableSeeder extends Seeder {

    public function run()
    {

        $pass = Hash::make('admin123456');

        DB::table('users')->insert(array(
            'name'=>'admin',
            'email' => 'admin@admin.com',
            'password'  => $pass,
            'activo'=>1,
            'id_rol'=>1,
            'is_logged'=>false
        ));
    }

}
