<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('roles')->insert([
            'nombre'=>'administrador',
            'status'=>1,
        ]);

        DB::table('roles')->insert([
            'nombre'=>'consulta',
            'status'=>1,
        ]);
    }
}
