@extends('adminlte.admin')
@section('content')

<h1>Editar usuario: {{$usuario->name}}</h1>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-success">
                <div class="box-body">
                        <form action="/usuarioEditado" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$usuario->id}}">
                                <div class="form-group">
                                    <label for="">Usuario</label>
                                    <input type="text" class="form-control" name="name" value=" {{$usuario->name}}"required >
                                </div>
                                <div class="form-group">
                                    <label for="">Correo</label>
                                    <input type="text" class="form-control" name="email" value=" {{$usuario->email}}" required>
                                    
                                </div>
                                <div class="form-group">
                                    <label for="">Contraseña</label>
                                    <input type="password" class="form-control" name="password" value=" {{$usuario->password}}"required >
                                    <br>
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                </div>
                                
                        </form>   
                </div>
            </div>
        </div>
    </div>
</div>

<script>
        $(document).ready(function(){                
                      $('.sidebar-menu li.active').removeClass('active');
                        $("#li_usuarios").addClass('active');
    
                })
    </script>

@endsection