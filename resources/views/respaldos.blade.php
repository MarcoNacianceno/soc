@extends('adminlte.admin')
@section('content')
@if(session()->has('mensaje'))
<div class="row">
    <div class="col-xs-12">
    <div class="alert alert-danger" role="alert">
        {{session('mensaje')}}
    </div>
    </div>
</div>
@endif

<h1>Respaldos <small>MSSP</small></h1>
<div class="row">
    <div class="col-xs-12">
    </div>
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-body table-responsive">
                    <button type="button" class="btn-lg btn-primary" id="btn_subir">Subir archivo <i class="fa fa-file"></i></button>
                <table id="tableArchivo" class="table table-bordered">
                    <thead>
                        <th>Nombre</th>
                        <th>Tamaño</th>
                        <th>Descargar</th>
                    </thead>
                    <tbody></tbody>
                    <tfoot></tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-default fade" id="modal-archivo">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Subir archivo</h4>
            </div>
            <input type="hidden" id="id_cliente_modal">
            <div class="modal-body">
                <form action="/subirArchivo" method="POST" enctype="multipart/form-data" id="my-dropzone" class="dropzone">
                    <div class="dz-message" style="height:200px;">
                            Drop your files here
                        </div>
                        <div class="dropzone-previews"></div>
                    
                        <button type="submit" class="btn btn-success" id="submit">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->

<script src="{{URL::asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{URL::asset('adminlte/bower_components/dropzone/dist/dropzone.js')}}"></script>

<script>
    var table = $('#tableArchivo').DataTable({
        "ajax": {
            "url":"/getAllFiles",
            "dataSrc":"data"
        } ,
        "paging": false,
        "language":{
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }

        },


            "columns": [
         
            {
              "render":function(data,type,row){
                            return  row.name + '.' + row.extension;
                    }
            },
            { "data": "size" },
            {
              "render":function(data,type,row){
                            return '<a href="' + row.path_descarga + '">Descargar Archivo</a>';
                    }
            },


            ],
    });

    $('.sidebar-menu li.active').removeClass('active');
    $("#li_respaldos").addClass('active');



    $('#btn_subir').on('click',function(){
        $("#modal-archivo").modal();

    });


        Dropzone.options.myDropzone = {
            parallelUploads: 10,
            autoProcessQueue: false,
            uploadMultiple: true,
            maxFilesize: 100,
            maxFiles: 10,
            paramName: "archivo",
            method: "POST",
            url: "/subirArchivo",
            sending: function(file, xhr, formData) {
                formData.append("_token", "{{ csrf_token() }}");
            },

            init: function() {
                var submitBtn = document.querySelector("#submit");
                myDropzone = this;

                submitBtn.addEventListener("click", function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    myDropzone.processQueue();
                    
                });
                this.on("addedfile", function(file) {
                    console.log("file uploaded");
                });

                this.on("complete", function(file) {
                    myDropzone.removeFile(file);
                });

                this.on("success", function()
                {
                    myDropzone.processQueue.bind(myDropzone);
                    table.ajax.reload(null, false);
                });
            }
        };
</script>
@endsection
