@extends('adminlte.admin')

@section('content')



@if(session()->has('mensaje'))
<div class="row">
    <div class="col-xs-12">
    <div class="alert alert-danger" role="alert">
        {{session('mensaje')}}
    </div>
    </div>
</div>
@endif

<h1>Clientes <small>MSSP</small></h1>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">

            <div class="box-body table-responsive">
                <table id="tableCliente" class="table table-bordered">
                    <thead>
                        <th>Empresa</th>
                        <th>Sitio</th>
                        <th>IP o Dynamic DNS</th>
                        <th>Fabricante</th>
                        <th>Serial</th>
                        <th>Contacto</th>
                        <th>Telefono</th>
                        <th>Email</th>
                        <th>Notas</th>
                        <th></th>
                    </thead>
                    <tbody>
                        @foreach ($clientes as $key => $cliente)
                        
                        <tr id="tr_{{$cliente->idCliente}}">
                            <td>{{$cliente->nomCliente}}</td>
                            <td>{{$cliente->sucursal}}</td>
                            <td>{{$cliente->dirIP}}</td>
                            <td>{{$cliente->fabricante}}</td>
                            <td>{{$cliente->serial}}</td>
                                        <td>{{$cliente->nomContacto}}</td>
                            <td>{{$cliente->telContacto}}</td>
                            <td>{{$cliente->emailContacto}}</td>
                            <td>{{$cliente->notas}}</td>
                            
                            @if(Auth::user()->id_rol == 1)
                            <td>
                                    <div class="btn-group">
                                        <a href="/editarCliente/{{$cliente->idCliente}}" class="btn btn-default flat " role="button"><i class="fa fa-edit"></i></a>
                                        <button type="button" class="btn btn-danger flat" data-target="#modal-danger" onclick="mostrarModal({{$cliente->idCliente}})"><i class="fa fa-trash"></i></button>
                                        
                                    </div>
                             
                            </td>
                            @else

                             <td>
                                    
                             
                            </td>

                            @endif
                        </tr>     
                     
                            
                        
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-danger fade" id="modal-danger">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Advertencia</h4>
            </div>
            <input type="hidden" id="id_cliente_modal">
            <div class="modal-body">
              <p>Esta seguro de eliminar este cliente</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-outline" id="confirm">Eliminar</button>
            </div>
        </div>
          <!-- /.modal-content -->
    </div>
        <!-- /.modal-dialog -->
</div>
      <!-- /.modal -->

<script src="{{URL::asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
    var table = $('#tableCliente').DataTable({
        "paging": false,
        "bInfo" : false,
        "language":{
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }

        }
    });


    function mostrarModal(id_cliente){
        $("#id_cliente_modal").val(id_cliente);
        $("#modal-danger").modal();
    }

    $("#confirm").on('click',function(){

        id_cliente = $("#id_cliente_modal").val();        
        var url = "/borrarCliente";
      
               $.ajax({
                   url: url,
                   type: "POST",
                   data: {
                       idCliente:id_cliente,
                       "_token": "{{ csrf_token() }}",
                    },
                   success: function(response){
                       //todo salio bien
                       $("#tr_" + id_cliente).remove();
                       $('#modal-danger').modal('hide');
                      
                   },
                   error: function(response){
                       //hubo un error
                       console.log(response);
                   }
                   })
    });

    

        $("#home").addClass('active');
    
   



</script>
@endsection
