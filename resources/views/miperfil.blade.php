@extends('adminlte.admin')
@section('content')

<h1>Mi Cuenta</h1>
<div class="row">

    <div class="col-md-8 col-md-offset-2">

            <div class="box box-widget widget-user-2">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-primary">
                            <div class="widget-user-image">
                                    <img class="img-circle" src="{{URL::asset('adminlte/img/user.png')}}" alt="User Avatar">
                            </div>
                                            <!-- /.widget-user-image -->
                      <h3 class="widget-user-username">{{Auth::user()->name}}</h3>
                      @if(Auth::user()->id_rol==1)
                      <h5 class="widget-user-desc">Rol: Administrador</h5>
                      @else
                      <h5 class="widget-user-desc">Rol: Consulta</h5>
                      
                      
                      @endif
                      
                      
                    </div>
                    <div class="box-footer no-padding">
                      <ul class="nav nav-stacked">
                        <li><a>{{Auth::user()->email}}</a></li>
                        <li><a href="/editarUsuario"><div class="btn btn-flat btn-success">Editar</div></a></li>
                      </ul>
                    </div>
                  </div>
    </div>
</div>

<script>
    $(document).ready(function(){                
                  $('.sidebar-menu li.active').removeClass('active');
                    $("#li_miCuenta").addClass('active');

            })
</script>
@endsection