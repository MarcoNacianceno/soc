
@extends('adminlte.admin')
@section('content')
<link rel="stylesheet" href="{{URL::asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
<link rel="stylesheet" href="{{URL::asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

<h2>Registro de activides<small> MSSP </small></h2>
<div class="row">
  <div class="col-md-4 col-md-offset-4">
    <div class="form-group" autocomplete="off">
      <label>Date range:</label>
      <div class="input-group">
        <div class="input-group-addon">
          <i class="fa fa-calendar"></i>
        </div>
        <input type="text" class="form-control pull-right" id="reservation" required >
      </div>
      <br>
      <button type="button" class="btn btn-warning" id="saveDates" >Buscar</button>
      <!-- /.input group -->
    </div>
  </div>
</div>

<div id="timeline"></div>


<script src="{{URL::asset('adminlte/bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{URL::asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script src="{{URL::asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>


<script type="text/javascript">
  var startDate;
  var endDate ;
  $('#reservation').daterangepicker({},function(start,end){
      $('#reservation').html(start.format('YYYY-DD-MM') + ' - ' + end.format('YYYY-DD-MM'));
      startDate = start.format('YYYY-MM-DD');
      endDate = end.format('YYYY-MM-DD');
      }
    );
    
    $(document).ready(function(){                
                  $('.sidebar-menu li.active').removeClass('active');
                    $("#li_actividades").addClass('active');

            });


    $('#saveDates').click(function(){
      
      var fechaTemp = 0;
      var url = "/fechasActividad";
      var timeline = "";
      $.ajax({
        url: url,
        type: "POST",
        data:{
          startDate: startDate ,
          endDate: endDate ,
          "_token": "{{ csrf_token() }}",
        },
        success:function(response){
          if(response.length > 0){
            response.forEach(function(respuesta, index){
              if(index == 0){
                fechaTemp = respuesta.created_at.substring(0,10);
                timeline+=
                '<div class="row">'+               
                    '<div class="col-md-10 col-md-offset-1">'+
                      '<ul class="timeline">'+
                          '<li class="time-label">'+
                            '<span class="bg-yellow">'+ respuesta.created_at.substring(0,10) + '</span>'+                          
                          '</li>'+
                          '<li>'+
                          '<div class="timeline-item">'+
                            '<span class="time"><i class="fa fa-clock-o"></i>'+ respuesta.created_at.substring(12,16) +'</span>'+
                            '<h3 class="timeline-header"> <b>'+respuesta.actividad+'</b> </h3>'+
                            '<div class="timeline-body">'+ respuesta.descripcion +'</div>'+
                            '<div class="timeline-footer">  Responsable: '+
                              '<b>'+respuesta.name+'</b>'+
                            '</div>'+
                          '</div>'+
                        '</li>'+      
                      '</ul>'+
                    '</div>'+
                '</div>';
              }
              else{
                if(fechaTemp == respuesta.created_at.substring(0,10)){
                  timeline+=
                        '<div class="row">'+               
                          '<div class="col-md-10 col-md-offset-1">'+
                            '<ul class="timeline">'+
                               '<li>'+
                                '<div class="timeline-item">'+
                                  '<span class="time"><i class="fa fa-clock-o"></i>'+ respuesta.created_at.substring(12,16) +'</span>'+
                                  '<h3 class="timeline-header"> <b>'+respuesta.actividad+'</b> </h3>'+
                                  '<div class="timeline-body">'+ respuesta.descripcion +'</div>'+
                                  '<div class="timeline-footer"> Responsable: '+
                                    '<b>'+respuesta.name+'</b>'+
                                  '</div>'+
                                '</div>'+
                              '</li>'+
                            '</ul>'+
                          '</div>'+ 
                        '</div> '
                }
                else
                {
                  timeline+=
                          '<div class="row">'+               
                            '<div class="col-md-10 col-md-offset-1">'+
                              '<ul class="timeline">'+
                                '<li class="time-label">'+
                                  '<span class="bg-yellow">'+ respuesta.created_at.substring(0,10) + '</span>'+                          
                                '</li>'+
                                '<li>'+
                                  '<div class="timeline-item">'+
                                    '<span class="time"><i class="fa fa-clock-o"></i>'+ respuesta.created_at.substring(12,16) +'</span>'+
                                    '<h3 class="timeline-header"> <b>'+respuesta.actividad+'</b> </h3>'+
                                    '<div class="timeline-body">'+ respuesta.descripcion +'</div>'+
                                    '<div class="timeline-footer"> Responsable : '+
                                      '<b>'+respuesta.name+'</b>'+
                                    '</div>'+
                                  '</div>'+
                                '</li>'+
                                      
                              '</ul>'+
                            '</div>'+ 
                          '</div> ';
                          fechaTemp = respuesta.created_at.substring(0,10);
                }
              }
              document.getElementById("timeline").innerHTML = timeline;
            })
          }
        },
        error:function(response){
          console.log(response);

        }

      })
    })
</script>
@endsection