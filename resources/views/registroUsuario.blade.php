@extends('adminlte.admin')

@section('content')

<h1>Registro de Usuarios</h1>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
                
            <div class="box-body">
                    <h3>Datos de Usuario</h3>
                <form action="agregarUsuario" method="POST" autocomplete="off">
                    {{ csrf_field() }}
                    <input type="text" class="form-control" name="name" placeholder="Nombre Usuario"  required>
                    <input type="email" class="form-control" name="email" placeholder="Email" required>
                    <input type="password" class="form-control" name="password" placeholder="Password" required>
                    <br>
                    <button type="submit" class="btn btn-primary">Crear Usuario</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){                
                  $('.sidebar-menu li.active').removeClass('active');
                    $("#li_usuarios").addClass('active');

            })
</script>


@endsection