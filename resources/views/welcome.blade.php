<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>MSSP</title>
        <link rel="stylesheet" href="{{URL::asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
 
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  
            <link rel="stylesheet" href="{{URL::asset('adminlte/css/skins/skin-black.min.css')}}">

            

            <link rel="stylesheet" href="{{URL::asset('adminlte/css/mis-estilos.css')}}">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <script src="{{URL::asset('adminlte/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
        <script src="{{URL::asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- AdminLTE App -->
        <script src="{{URL::asset('adminlte/js/adminlte.min.js')}}"></script>


        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                    @if(session()->has('mensaje'))
                    <div class="row justify-content-center">
                        <div class="col-xs-12">
                        <div class="alert alert-danger" role="alert">
                            {{session('mensaje')}}
                        </div>
                        </div>
                    </div>
                    @endif
                <div class="title m-b-md">
                        <img src="{{URL::asset('adminlte/img/logo.png')}}" style="width: 5em; " ><br>
                        <a class="btn btn-primary" href="{{ route('login') }}" style="color:fff"><b>Login</b></a>
                        
                </div>
            </div>
        </div>
    </body>
</html>
