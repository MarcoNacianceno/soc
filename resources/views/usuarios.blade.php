@extends('adminlte.admin')

@section('content')

<h1>Usuarios <small>MSSP</small></h1>
<div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
    
                <div class="box-body  table-responsive">
                    <table id="tableUsuario" class="table table-bordered ">
                        <thead>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Creado el</th>
                            <th>Modificado el</th>
                            <th></th>
                        </thead>
                        <tbody>
                            @foreach ($usuarios as $key => $usuario) 
                            <tr id="tr_{{$usuario->id}}">
                                <td>{{$usuario->id}}</td>
                                <td>{{$usuario->name}}</td>
                                <td>{{$usuario->email}}</td>
                                <td>{{$usuario->created_at}}</td>
                                <td>{{$usuario->updated_at}}</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="/editarUsuario/{{$usuario->id}}" class="btn btn-default flat" role="button"><i class="fa fa-edit"></i></a>
                                        <button type="button" class="btn btn-danger flat" data-target="#modal-danger" onclick="mostrarModal({{$usuario->id}})"><i class="fa fa-trash"></i></button>                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-danger fade" id="modal-danger">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Advertencia</h4>
                </div>
                <input type="hidden" id="id_usuario_modal">
                <div class="modal-body">
                  <p>Esta seguro de eliminar este usuario
                  </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-outline" id="confirm">Eliminar</button>
                </div>
            </div>
              <!-- /.modal-content -->
        </div>
            <!-- /.modal-dialog -->
    </div>
          <!-- /.modal -->

    <script src="{{URL::asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        var tableUsuarios = $('#tableUsuario').DataTable();

        function mostrarModal(id){
            $("#id_usuario_modal").val(id);
            $("#modal-danger").modal();
        }

        $("#confirm").on('click',function(){
            id_usuario = $("#id_usuario_modal").val();        
            var url = "/borrarUsuario";
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        id:id_usuario,
                        "_token": "{{ csrf_token() }}",
                        },
                    success: function(response){
                        //todo salio bien
                        $("#tr_" + id_usuario).remove();
                        $('#modal-danger').modal('hide');
                        
                    },
                    error: function(response){
                        //hubo un error
                        console.log(response);
                    }
                    })
            });

            $(document).ready(function(){                
                  $('.sidebar-menu li.active').removeClass('active');
                    $("#li_usuarios").addClass('active');

            })
   </script>
@endsection