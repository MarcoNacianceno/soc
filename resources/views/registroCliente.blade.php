@extends('adminlte.admin')

@section('content')

<h1>Registro de Cliente</h1>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-body">
                    <form action="regCliente" method="POST">
                            {{ csrf_field() }}
                            <h3>Datos del Cliente</h3>
                            <input type="text" class="form-control" name="nomCliente" placeholder="Nombre Cliente" required>
                            <input type="text" class="form-control" name="sucursal" placeholder="Sucursal" required>
                            <input type="text" class="form-control" name="ip_url" placeholder="Direccion IP / URL" required>
                            <input type="text" class="form-control" name="fabricante" placeholder="Fabricante">
                            <input type="text" class="form-control" name="serial" placeholder="Serial">
                            <textarea type="text" class="form-control" name="notas" placeholder="Notas del Cliente"></textarea>
                            <h3>Contacto</h3>
                            <input type="text" class="form-control" name="nomContactoCliente" placeholder="Nombre de Contacto" required>
                            <input type="text" class="form-control" name="telContactoCliente" placeholder="Telefono de Contacto"  required>
                            <input type="email" class="form-control" name="emailContactoCliente" placeholder="Email de Contacto" required>
                            <br>
                            <button type="submit" class="btn btn-success">Crear Cliente</button>
                    </form>
            </div>
        </div>
    </div class="col-xs-12">
</div>

<script src="{{URL::asset('adminlte/plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{URL::asset('adminlte/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>
<script>
$(function(){
    $('[data-mask]').inputmask()
});

</script>
@endsection()
