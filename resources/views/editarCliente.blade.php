@extends('adminlte.admin')
@section('content')

<h1>Editar Cliente: {{$cliente->nomCliente}}</h1>

<div class="container-fluid">
    <div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-body">

                <h3>Datos Cliente</h3>
                    <form action="/clientEditado" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="idCliente" value="{{$cliente->idCliente}}">
                            <div class="form-group">
                                <label for="">Empresa</label>
                                <input type="text" class="form-control" name="nomCliente" value=" {{$cliente->nomCliente}}"  required>
                            </div>
                            <div class="form-group">
                                <label for="">Sitio</label>
                                <input type="text" class="form-control" name="sucursal" value=" {{$cliente->sucursal}}"  required>
                            </div>
                            <div class="form-group">
                                <label for="">IP o Dynamic DNS</label>
                                <input type="text" class="form-control" name="ip_url" value="{{$cliente->dirIP}} "  required>
                            </div>
                            <div class="form-group">
                                <label for="">Fabricante</label>
                                <input type="text" class="form-control" name="fabricante" value=" {{$cliente->fabricante}}" >
                            </div>
                            <div class="form-group">
                                <label for="">Serial</label>
                                <input type="text" class="form-control" name="serial" value=" {{$cliente->serial}}" >
                            </div>
                            <div class="form-group">
                                <label for="">Notas</label>
                                <textarea type="text" class="form-control" name="notas">{{$cliente->notas}}</textarea>
                            </div>

                          <h3>Datos de Contacto</h3>

                            <div class="form-group">
                                <label for="">Nombre Contacto</label>
                                <input type="text" name="nomContactoCliente" class="form-control" value="{{$cliente->nomContacto}}" required>
                            </div>
                            <div class="form-group">
                                  <label for="">Telefono</label>
                                <input type="text" name="telContactoCliente" class="form-control" value="{{$cliente->telContacto}}"  required>
                            </div>
                            <div class="form-group">
                                <label for="">E-mail</label>
                                <input type="email" name="emailContactoCliente" class="form-control" value="{{$cliente->emailContacto}}"  required>
                            </div>
                            
                            <button type="submit" class="btn btn-primary">Editar Cliente</button>
                        </form>

            </div>
        </div>
    </div>
</div>
</div>

<script src="{{URL::asset('adminlte/plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{URL::asset('adminlte/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>
<script>
$(function(){
    $('[data-mask]').inputmask()
});

</script>
@endsection